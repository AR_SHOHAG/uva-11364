#include <cstdio>
#include <cmath>
#include <iostream>
using namespace std;

int main()
{
    int t, n, i, mx=0, mn=100;
    scanf("%d", &t);

    for(int j=0; j<t; ++j){
        scanf("%d", &n);
        int arr[n];
        for(i=0; i<n; ++i){
            scanf("%d", &arr[i]);

            if(arr[i]>mx){
                mx=arr[i];
            }

            if(arr[i]<mn)
                mn=arr[i];

        }
        printf("%d\n", (mx-mn)*2);
        mx=0, mn=100;
    }

    return 0;
}
